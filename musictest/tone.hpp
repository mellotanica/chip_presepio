#ifndef TONE_H
#define TONE_H

#include <stdint.h>

void init_tone();
void close_tone();

void tone(uint8_t _pin, unsigned int frequency, unsigned int duration_ms);

void tone(uint8_t _pin, unsigned int frequency);

void noTone(uint8_t _pin);

unsigned long millis();

#endif
