#ifndef Music_h
#define Music_h

#include <iostream>
#include "Note.hpp"

template <int pin, unsigned long next_song_mean_ms = 30000, unsigned long next_song_sigma_ms = 10000>
class Music{
   public:
    Music(Song* songs, int songs_count, unsigned long update_time_ms = 10)
          : songs(songs),
          songs_count(songs_count),
          current_song(-1)
    {
    }

    void schedule_next_song()
    {
        current_song++;
        if(!ended()) {
            songs[current_song].rewind();
            next_song_deadline = millis() + 500;
            std::cout << "scheduled next song: " << songs[current_song].get_name() << "\n";
        }
    }

    void setup()
    {
        for(int i = 0; i < songs_count; ++i) {
            songs[i].setup();
        }
        schedule_next_song();
    }

    bool ended() {
        return current_song >= songs_count;
    }

    void update()
    {
        if(!ended()) {
            if (songs[current_song].ended()) {
                noTone(pin);
                schedule_next_song();
            } else if (songs[current_song].playing() || millis() > next_song_deadline) {
                // go on with current song playback or start new song
                songs[current_song].update(pin);
            }
        }
    }

   protected:
    Song* songs;
    int songs_count;

    int current_song;
    unsigned long next_song_deadline;
};

#endif  // Music_h
