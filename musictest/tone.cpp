#include "tone.hpp"

#include "midi_io.h"
#include <math.h>
#include <portmidi.h>
#include <chrono>

static const float semitones_per_octave = 12.0f;
static const float a4_frequency = 440.0f;

unsigned short last_note;
MIDI_io midi_io;
int output_device;

void init_tone() {
    midi_io.create_virtual_output_device("musictest");
    midi_io.list_devices();

    std::cout << "Give output device number: ";
    cin >> output_device;
    midi_io.set_output_device(output_device);

    midi_io.initialise();
}

unsigned short note_from_freq(unsigned int frequency) {
  float note = 69.0f + logf(frequency / a4_frequency) / logf(2.0f) * semitones_per_octave;
  return round(note);
}

void close_tone() {
    midi_io.finalise();
}

void tone(uint8_t _pin, unsigned int frequency, unsigned int duration_ms) {
    unsigned short note = note_from_freq(frequency);
    PmEvent event;

    event.message = Pm_Message(0x80, last_note, 0);
    event.timestamp = 0;
    midi_io.write_event(&event);

    if(frequency > 0) {
        event.message = Pm_Message(0x90, note, 127);
        midi_io.write_event(&event);
        last_note = note;
    }
}

void tone(uint8_t _pin, unsigned int frequency) {
    tone(_pin, frequency, 0);
}

void noTone(uint8_t _pin) {
    tone(_pin, 0, 0);
}

unsigned long millis() {
    namespace sc = std::chrono;

    auto time = sc::system_clock::now(); // get the current time

    auto since_epoch = time.time_since_epoch(); // get the duration since epoch

    // I don't know what system_clock returns
    // I think it's uint64_t nanoseconds since epoch
    // Either way this duration_cast will do the right thing
    auto millis = sc::duration_cast<sc::milliseconds>(since_epoch);

    return millis.count();
}
