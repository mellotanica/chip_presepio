#include <chrono>
#include <thread>
#include "Music.hpp"
#include "musica.h"
#include "tone.hpp"

#define ARRAY_LEN(a) (sizeof(a) / sizeof(a[0]))
Music<0> musica(songs, ARRAY_LEN(songs));

int main (int argc, char *argv[]) {
    init_tone();
    musica.setup();

    while(!musica.ended()) {
        musica.update();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    noTone(0);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

    close_tone();

    return 0;
}
