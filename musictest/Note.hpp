#ifndef Note_h
#define Note_h

#include "tone.hpp"

enum NoteFreq {
    N_pause = 0,
    N_end   = 1,
    N_B0    = 31,
    N_C1    = 33,
    N_CS1   = 35,
    N_D1    = 37,
    N_DS1   = 39,
    N_E1    = 41,
    N_F1    = 44,
    N_FS1   = 46,
    N_G1    = 49,
    N_GS1   = 52,
    N_A1    = 55,
    N_AS1   = 58,
    N_B1    = 62,
    N_C2    = 65,
    N_CS2   = 69,
    N_D2    = 73,
    N_DS2   = 78,
    N_E2    = 82,
    N_F2    = 87,
    N_FS2   = 93,
    N_G2    = 98,
    N_GS2   = 104,
    N_A2    = 110,
    N_AS2   = 117,
    N_B2    = 123,
    N_C3    = 131,
    N_CS3   = 139,
    N_D3    = 147,
    N_DS3   = 156,
    N_E3    = 165,
    N_F3    = 175,
    N_FS3   = 185,
    N_G3    = 196,
    N_GS3   = 208,
    N_A3    = 220,
    N_AS3   = 233,
    N_B3    = 247,
    N_C4    = 262,
    N_CS4   = 277,
    N_D4    = 294,
    N_DS4   = 311,
    N_E4    = 330,
    N_F4    = 349,
    N_FS4   = 370,
    N_G4    = 392,
    N_GS4   = 415,
    N_A4    = 440,
    N_AS4   = 466,
    N_B4    = 494,
    N_C5    = 523,
    N_CS5   = 554,
    N_D5    = 587,
    N_DS5   = 622,
    N_E5    = 659,
    N_F5    = 698,
    N_FS5   = 740,
    N_G5    = 784,
    N_GS5   = 831,
    N_A5    = 880,
    N_AS5   = 932,
    N_B5    = 988,
    N_C6    = 1047,
    N_CS6   = 1109,
    N_D6    = 1175,
    N_DS6   = 1245,
    N_E6    = 1319,
    N_F6    = 1397,
    N_FS6   = 1480,
    N_G6    = 1568,
    N_GS6   = 1661,
    N_A6    = 1760,
    N_AS6   = 1865,
    N_B6    = 1976,
    N_C7    = 2093,
    N_CS7   = 2217,
    N_D7    = 2349,
    N_DS7   = 2489,
    N_E7    = 2637,
    N_F7    = 2794,
    N_FS7   = 2960,
    N_G7    = 3136,
    N_GS7   = 3322,
    N_A7    = 3520,
    N_AS7   = 3729,
    N_B7    = 3951,
    N_C8    = 4186,
    N_CS8   = 4435,
    N_D8    = 4699,
    N_DS8   = 4978,
};

struct Note {
    NoteFreq frequency;
    unsigned short duration;
    unsigned short division;
    unsigned long duration_ms;

    inline void set_bpm(unsigned long bpm)
    {
        if (duration_ms == 0 && duration > 0 && division > 0) {
            duration_ms = (4 * duration * (60 * (1000 / bpm)) / division);
        }
    }
    inline void play(int pin) { tone(pin, frequency, duration_ms); }
};

class Song {
   public:
    Song(const char* name, unsigned int bpm, Note* notes, unsigned int inter_note_pause = 10)
        : name(name), bpm(bpm), notes(notes), inter_note_pause(inter_note_pause)
    {
    }

    void setup()
    {
        for (int i = 0; notes[i].frequency != N_end; ++i) {
            notes[i].set_bpm(bpm);
        }
    }

    inline void rewind()
    {
        deadline = 0;
        current  = -1;
    }
    inline bool ended() { return (current >= 0 && notes[current].frequency == N_end); }
    inline bool elapsed() { return (millis() > deadline); }
    inline bool playing() { return deadline > 0; }

    void update(int pin)
    {
        if (!ended() && elapsed()) {
            // play to next note
            current++;
            if (!ended()) {
                deadline = millis() + notes[current].duration_ms + inter_note_pause;
                notes[current].play(pin);
            }
        }
    }

    inline const char* get_name() { return name; }

   protected:
    const char* name;
    unsigned int bpm;
    Note* notes;
    unsigned int inter_note_pause;
    int current;
    unsigned long deadline;
};

#endif  // Note_h
