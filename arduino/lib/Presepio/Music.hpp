#ifndef Music_h
#define Music_h

#include "Base.hpp"
#include "Note.hpp"
#include <Gaussian.h>

template <int pin, unsigned long next_song_mean_ms = 30000, unsigned long next_song_sigma_ms = 10000>
class Music : public Component {
   public:
    Music(Song* songs, int songs_count, unsigned long update_time_ms = 10)
        : Component(update_time_ms),
          songs(songs),
          songs_count(songs_count),
          current_song(-1),
          next_song_distrib(next_song_mean_ms, next_song_sigma_ms)
    {
    }

    void schedule_next_song()
    {
        current_song = random(songs_count);
        songs[current_song].rewind();
        next_song_deadline = millis() + floor(next_song_distrib.random());
    }

    void setup()
    {
        pinMode(pin, OUTPUT);
        for(int i = 0; i < songs_count; ++i) {
            songs[i].setup();
        }
        schedule_next_song();
    }

    void update()
    {
        if (enabled) {
            if (songs[current_song].ended()) {
                noTone(pin);
                schedule_next_song();
            } else if (songs[current_song].playing() || millis() > next_song_deadline) {
                // go on with current song playback or start new song
                songs[current_song].update(pin);
            }
        } else if (songs[current_song].playing()) {
            // if disabled while playing, conclude current song so that after wakeup a new one will be scheduled
            songs[current_song].update(pin);
        } else {
            noTone(pin);
        }
    }

   protected:
    Song* songs;
    int songs_count;

    int current_song;
    unsigned long next_song_deadline;

    Gaussian next_song_distrib;
};

#endif  // Music_h
