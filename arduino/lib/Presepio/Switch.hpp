#ifndef Switch_h
#define Switch_h

#include "Arduino.h"
#include "Base.hpp"

template <int pin, bool default_state = true, bool on_state = true>
class Switch : public Component {
   public:
    Switch(unsigned long update_time_ms = 100)
        : Component(update_time_ms),
          on_value(on_state ? HIGH : LOW),
          off_value(on_state ? LOW : HIGH),
          current_state(default_state)
    {
    }

    virtual inline void setup() {
        pinMode(pin, OUTPUT);
        set_state(current_state);
    }
    virtual inline void update() {}

    virtual void set_enabled(bool enabled) {
        Component::set_enabled(enabled);
        set_state(current_state);
    }

    virtual inline void set_state(bool state)
    {
        current_state = state;
        digitalWrite(pin, (is_enabled() && current_state ? on_value : off_value));
    }
    inline void turn_on() { set_state(true); }
    inline void turn_off() { set_state(false); }
    inline void toggle() { set_state(!current_state); }

   protected:
    int on_value;
    int off_value;
    bool current_state;
};

#endif  // Switch_h
