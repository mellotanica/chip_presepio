#ifndef Sensor_h
#define Sensor_h

#include "Arduino.h"
#include "Base.hpp"

using SensorCB = void (*)(bool on);

enum SensorDetection {
    raising = HIGH,
    falling = LOW,
    both,
};

template <int pin, SensorDetection type, SensorCB action_callback, int on_state = HIGH, unsigned long on_debounce_ms = 100, unsigned long off_debounce_ms = 100>
class Sensor : public Component {
   public:
    Sensor(unsigned long update_time_ms = 100) : Component(update_time_ms), last_state(-1), debounce_deadline(-1) {}

    virtual inline void setup() { pinMode(pin, INPUT); }

    virtual void update()
    {
        if (is_enabled()) {
            int state = digitalRead(pin);

            if (state != last_state) {
                // value changed, reload debounce timer
                if (state == on_state) {
                    debounce_deadline = millis() + on_debounce_ms;
                } else {
                    debounce_deadline = millis() + off_debounce_ms;
                }
                last_state = state;
            } else {
                // value stationary
                if(debounce_deadline > 0 && millis() > debounce_deadline) {
                    // if debounce timer was running and has elapsed, the value passed
                    // the debounce check, run the action if needed
                    if (type == both || type == state) {
                        action_callback(state == on_state);
                    }
                    // reset debounce time to avoid running multiple triggers
                    debounce_deadline = 0;
                }
            }
        }
    }

   protected:
    int last_state;
    unsigned long debounce_deadline;
};

#endif  // Sensor_h
