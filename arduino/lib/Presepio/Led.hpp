#ifndef Led_h
#define Led_h

#include "Base.hpp"
#include <Gaussian.h>

template <int pin>
class Led : public Component {
   public:
    Led(unsigned long update_time_ms = 20) : Component(update_time_ms), brightness(0), deadline(0), deadline_set(false) {}

    void set_brightness(int percent, unsigned long duration_ms = 0)
    {
        if (percent < 0) {
            brightness = 0;
        } else if (percent > 100) {
            brightness = 100;
        } else {
            brightness = percent;
        }
        if (duration_ms > 0) {
            deadline     = millis() + duration_ms;
            deadline_set = true;
        } else {
            deadline_set = false;
        }
    }
    inline int get_brightness() { return brightness; }

    inline bool elapsed() { return (deadline_set && (millis() > deadline)); }

    virtual void setup() { pinMode(pin, OUTPUT); }
    virtual void update()
    {
        if (!is_enabled() || elapsed()) {
            digitalWrite(pin, LOW);
        } else {
            analogWrite(pin, brightness);
        }
    }

   protected:
    int brightness;
    unsigned long deadline;
    bool deadline_set;
};

template <int pin>
class Star : public Led<pin> {
   public:
    Star(unsigned long update_time_ms = 20) : Led<pin>(update_time_ms), current_angle(0)
    {
        angle_increment = floor((360.0 * update_time_ms) / sweep_time_ms);
        if(angle_increment < 1) {
            angle_increment = 1;
        }
    }

    void setup()
    {
        Led<pin>::setup();
        Led<pin>::set_brightness(min_brightness, 1);
    }
    void update()
    {
        if (Led<pin>::elapsed()) {
            int brightness = floor(sin(current_angle / 360.0 * 2 * PI) * brightness_delta);
            if (brightness < 0) {
                brightness = -brightness;
            }
            Led<pin>::set_brightness(min_brightness + brightness, Component::interval);
            current_angle = (current_angle + angle_increment) % 360;
        }
        Led<pin>::update();
    }

    const int min_brightness   = 30;
    const int max_brightness   = 100;
    const int brightness_delta = max_brightness - min_brightness;
    const int sweep_time_ms    = 3000;

   protected:
    int current_angle;
    int angle_increment;
};

template <int pin,
          unsigned int sweep_mean      = 70,
          unsigned int sweep_sigma     = 20,
          unsigned int duration_mean   = 300,
          unsigned int duration_sigma  = 300,
          unsigned int restart_percent = 20>
class SweepingLed : public Led<pin> {
   public:
    SweepingLed(unsigned long update_time_ms = 20)
        : Led<pin>(update_time_ms),
          sweep_target(sweep_mean, sweep_sigma),
          sweep_duration(duration_mean, duration_sigma),
          sweeping(false),
          sweep_increment(0),
          target(0)
    {
    }

    void update()
    {
        if (sweeping) {
            int brightness = Led<pin>::get_brightness();
            if (brightness < target) {
                Led<pin>::set_brightness(brightness + sweep_increment);
                if (Led<pin>::get_brightness() >= target) {
                    sweeping = false;
                }
            } else {
                Led<pin>::set_brightness(brightness - sweep_increment);
                if (Led<pin>::get_brightness() <= target) {
                    sweeping = false;
                }
            }
        } else if (random(100) < restart_percent) {
            sweeping               = true;
            target                 = floor(sweep_target.random());
            if(target > 100) {
                target = 100;
            }
            if(target < 0) {
                target = 0;
            }
            float duration         = floor(sweep_duration.random());
            if(duration < 0) {
                duration = 0;
            }
            duration /= Component::interval;
            int delta = 1;
            if(Led<pin>::get_brightness() < target) {
                delta = target - Led<pin>::get_brightness();
            } else {
                delta = Led<pin>::get_brightness() - target;
            }
            sweep_increment        = ceil(delta / duration);
            if(sweep_increment < 1) {
                sweep_increment = 1;
            }
        }
        Led<pin>::update();
    }

   protected:
    Gaussian sweep_target;
    Gaussian sweep_duration;

    bool sweeping;
    int sweep_increment;
    int target;
};

template <int yellow_pin, int red_pin>
class Fire : public Component {
   public:
    Fire(unsigned long update_time_ms = 20)
        : Component(update_time_ms),
          yellow_distrib(yellow_mean, yellow_sigma),
          yellow_on_distrib(yellow_on_mean, yellow_on_sigma),
          yellow_update_percent(0)
    {
    }

    void setup()
    {
        yellow.setup();
        yellow.set_brightness(0, 1);
        red.setup();
    }

    void update()
    {
        if (is_enabled()) {
            // handle yellow led
            if (yellow.elapsed()) {
                if (random(100) < yellow_update_percent) {
                    yellow_update_percent = 0;
                    int duration          = floor(yellow_on_distrib.random());
                    if(duration < 1) {
                        duration = 1;
                    }

                    int brightness = yellow_mean - floor((yellow_distrib.random()) * yellow_mult);
                    if(brightness < 0) {
                        brightness = -brightness;
                    }
                    yellow.set_brightness(brightness, duration);
                } else {
                    yellow_update_percent += yellow_percent_increment;
                }
            }
        }
        red.update();
        yellow.update();
    }

    void set_enabled(bool enabled)
    {
        Component::set_enabled(enabled);
        yellow.set_enabled(enabled);
        red.set_enabled(enabled);
    }

    static const int red_ud_percent           = 30;
    const float yellow_mult                   = 0.8;
    static const int yellow_percent_increment = 1;

    static const int red_mean        = 80;
    static const int red_sigma       = 30;
    static const int red_sweep_mean  = 600;
    static const int red_sweep_sigma = 200;
    static const int yellow_mean     = 70;
    static const int yellow_sigma    = 20;
    static const int yellow_on_mean  = 70;
    static const int yellow_on_sigma = 40;

   protected:
    Led<yellow_pin> yellow;
    SweepingLed<red_pin, red_mean, red_sigma, red_sweep_mean, red_sweep_sigma, red_ud_percent> red;

    Gaussian yellow_distrib;
    Gaussian yellow_on_distrib;

    int yellow_update_percent;
};

#endif  // Led_h
