#ifndef Base_h
#define Base_h

#define ARRAY_LEN(a) (sizeof(a) / sizeof(a[0]))

#include <Thread.h>

class Component : public Thread {
   public:
    Component(unsigned long update_time_ms) : enabled(false) { interval = update_time_ms; }

    virtual void setup()  = 0;
    virtual void update() = 0;

    inline void enable() { set_enabled(true); };
    inline void disable() { set_enabled(false); };
    virtual void set_enabled(bool enabled) { this->enabled = enabled; }
    inline bool is_enabled() { return enabled; }

    virtual void run()
    {
        update();
        runned();
    }

   protected:
    bool enabled;
};

#endif  // Base_h
