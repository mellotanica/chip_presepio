#ifdef Musica_h
#error "Musica header can be included only once"
#else
#define Musica_h

#include "Note.hpp"

Song songs[] = {
{"Mario Theme", 90, (Note[]){
        {N_E4, 1, 16},
        {N_E4, 1, 8},
        {N_E4, 1, 16},
        {N_pause, 1, 16},
        {N_C4, 1, 16},
        {N_E4, 1, 8},
        {N_G4, 1, 4},
        {N_G3, 1, 4},
        {N_C4, 1, 8},
        {N_pause, 1, 16},
        {N_G3, 1, 16},
        {N_pause, 1, 8},
        {N_E3, 1, 8},
        {N_pause, 1, 16},
        {N_A3, 1, 16},
        {N_pause, 1, 16},
        {N_B3, 1, 16},
        {N_pause, 1, 16},
        {N_AS3, 1, 16},
        {N_A3, 1, 8},
        {N_G3, 1, 8},
        {N_E4, 1, 8},
        {N_G4, 1, 16},
        {N_A4, 1, 8},
        {N_F4, 1, 16},
        {N_G4, 1, 16},
        {N_pause, 1, 16},
        {N_E4, 1, 16},
        {N_pause, 1, 16},
        {N_C4, 1, 16},
        {N_D4, 1, 16},
        {N_B3, 1, 16},
        {N_end, 1, 8},
    }},
};

#endif
