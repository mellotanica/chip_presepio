#include "Arduino.h"
#include <Thread.h>
#include <ThreadController.h>
#include "Base.hpp"
#include "HardwareSerial.h"
#include "Led.hpp"
#include "Music.hpp"
#include "Sensor.hpp"
#include "Switch.hpp"
#include "musica.h"

enum leds {
    INPUT_PIR      = 13,
    SWITCH_POMPA   = A3,
    SWITCH_EXTRA   = A4,
    BUZZER_MUSICA  = 12,
    LED_FUOCO_R    = 10,
    LED_FUOCO_Y    = 9,
    LED_CUCINA     = 8,
    LED_TETTOIA    = 7,
    LED_STELLA     = 6,
    LED_LAMPIONE   = 5,
    LED_LAVANDERIA = A1,
    LED_EXTRA0     = 4,
    LED_EXTRA1     = 2,
    LED_EXTRA2     = A5,
};

Star<LED_STELLA> stella;
Fire<LED_FUOCO_Y, LED_FUOCO_R> fuoco;
SweepingLed<LED_LAMPIONE> lampione;
Switch<LED_LAVANDERIA> lavanderia;
Switch<SWITCH_POMPA, true, false> pompa;
Switch<LED_CUCINA> cucina;
Switch<LED_TETTOIA> tettoia;
Switch<LED_EXTRA0> extra0;
Switch<LED_EXTRA1> extra1;
Switch<LED_EXTRA2> extra2;
Switch<SWITCH_EXTRA, true, false> extra3;
Music<BUZZER_MUSICA> musica(songs, ARRAY_LEN(songs));

Component* components[] = {&stella,  &fuoco,  &lampione, &lavanderia, &pompa, &cucina,
                           &tettoia, &extra0, &extra1,   &extra2}; //,     &musica};

#define COMPONENTS_SIZE (sizeof(components) / sizeof(components[0]))

void ir_update(bool state)
{
    for (unsigned int c = 0; c < ARRAY_LEN(components); ++c) {
        components[c]->set_enabled(state);
    }
}

Sensor<INPUT_PIR, both, ir_update, HIGH, 100, 30000> motion;

Component* sensors[] = {&motion};

#define COMPONENTS_SIZE (sizeof(components) / sizeof(components[0]))

ThreadController controller;

void setup()
{
    randomSeed(analogRead(0));
    Serial.begin(9600);
    while (!Serial) {
        ;  // wait for serial port to connect. Needed for native USB port only
    }

    Serial.println("setup start");
    for (unsigned int c = 0; c < ARRAY_LEN(components); ++c) {
        components[c]->setup();
        controller.add(components[c]);
    }
    Serial.println("setup sensors");
    for (unsigned int c = 0; c < ARRAY_LEN(sensors); ++c) {
        sensors[c]->setup();
        sensors[c]->enable();
        controller.add(sensors[c]);
    }
    Serial.println("ready, start loop");
}

void loop()
{
    controller.run();
}
